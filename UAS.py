import math
x1 , y1= [int(x1) for x1 in input("Enter co-ordinates of start point (give space in between): ").split()]
x2 , y2= [int(x2) for x2 in input("Enter co-ordinates of end point(give space in between): ").split()]
x3 , y3 , r3= [int(x3) for x3 in input("Enter x,y,r for circle(give space in between): ").split()]
dx = x2 - x1
dy = y2 - y1
m = (dy/dx)
c= (y1 - (m*x1))
d= (abs((-1*m*x3) + y3 + (-1*c)))/math.sqrt((m**2)+1)
print (f"D is {d}")
print (f"C is {c}")
print(f"M is {m}")
if(d<r3):
    print("Two points of intersection")
    x4 = (-(math.sqrt(-x3**2*m**2+2*x3*y3*m-2*x3*c*m-y3**2+2*y3*c-c**2+m**2*r3**2+r3**2)) +x3+y3*m-c*m)/(m**2+1)
    x5 = ((math.sqrt(-x3**2*m**2+2*x3*y3*m-2*x3*c*m-y3**2+2*y3*c-c**2+m**2*r3**2+r3**2)) +x3+y3*m-c*m)/(m**2+1)
    y4 = (-m*(math.sqrt(-x3**2*m**2+2*x3*y3*m-2*x3*c*m-y3**2+2*y3*c-c**2+m**2*r3**2+r3**2))+x3*m+y3*m**2+c)/(m**2+1)
    y5 = (m*(math.sqrt(-x3**2*m**2+2*x3*y3*m-2*x3*c*m-y3**2+2*y3*c-c**2+m**2*r3**2+r3**2))+x3*m+y3*m**2+c)/(m**2+1)
    print(f"Points are {x4},{y4} and {x5},{y5}")
elif(d==r3):
    print("One point of intersection")
    x4 = ((math.sqrt(-x3**2*m**2+2*x3*y3*m-2*x3*c*m-y3**2+2*y3*c-c**2+m**2*r3**2+r3**2)) +x3+y3*m-c*m)/(m**2+1)
    y4 = (m*(math.sqrt(-x3**2*m**2+2*x3*y3*m-2*x3*c*m-y3**2+2*y3*c-c**2+m**2*r3**2+r3**2))+x3*m+y3*m**2+c)/(m**2+1)
    print(f"The point of intersection is {x4} , {y4}")
else:
    print("No intersection")

